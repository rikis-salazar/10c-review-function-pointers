// Idea: There are different ways to "wrap" a function pointer. Specifically we
// would like to have a type, or types, that allow us to define function
// pointers without having to carry all the parenthesis and stars associated
// to function pointers.

#include <iostream>     // std::cout, std::ostream
#include <vector>       // std::vector<...>
#include <functional>   // std::function<...>

using std::cout;
using std::vector;
using std::ostream;

int my_function(){ return 2017; }

int square_element( int x ) { return x*x; } 
int double_element( int x ) { return 2*x; }

// operator<< 
ostream& operator<<( ostream&, int (*)() );
ostream& operator<<( ostream&, const vector<int>& );

// Function that changes behavior according to the policy that is sent to it
void modify_elements_in_vector( vector<int>& , int (*)(int) );


// `modify_elements_in_vector` is such a long name. We can use a function
// pointer to shorten this name. However the syntax is ugly.
void (*meiv1)( vector<int>&, int (*)(int) ) = modify_elements_in_vector;
// We can now write `meiv1(...)` instead of `modify...vector(...)`.
// Notice that if we wanted another pointer, say `meiv2`, we would have to 
// resort to the same ugly notation:
//
//     void (*meiv2)( vector<int>&, int (*)(int) ) = modify_elements_in_vector;



// We can achieve a similar result using a "type definition". 
typedef void (*function_ptr_t)( vector<int>&, int (*)(int) ) ;
// This creates a "function pointer type" that we can use in the same way we use
// other primitives, e.g., `int`, or `double`. For example, the line
function_ptr_t meiv2 = modify_elements_in_vector;
// creates a function pointer `meiv2` that points to `modify...vector`. The
// advantage is that the "ugliness" is needed only once. If we want another
// function pointer, say `meiv3`, we can now write:
//
//     function_ptr_t meiv3 = modify_elements_in_vector;
//
// And the "ugliness" is gone!!! 



// Another possibility is the use of an "alias":
using other_function_ptr_t = void (*)( vector<int>&, int (*)(int) );
// And just as before, we can declare a function pointer and point to our
// function in the same way we declare and initialize a variable.
other_function_ptr_t meiv3 = meiv2;
// Notice that this is mostly for aesthetic purposes, as the compiler still gets
// to see the ugliness associated to function pointers.



// A different approach calls for the use of a 'wrapper class'. At some point in
// the future we will study similar wrapper classes. Needless to say that
// function pointers will play an important role then.
std::function<void( vector<int>&, int (*)(int) )> meiv4 = meiv3;
// This might still be "ugly", however it is fairly clear what `meiv4` is,
// namely, an instance of a class that can handle functions taking two
// parameters (a `vector<int>&`, and a function pointer), and whose return type
// is `void`. The use of this wrapper class requires the use of the <functional>
// library.


int main(){

    // Function pointer syntax
    int (*ptr1_to_my_function)();

    // Via `std::function<...>` 
    std::function<int(void)> ptr2_to_my_function;


    // Obtaining starting memory address of `my_function`
    ptr1_to_my_function = my_function;   // implicit
    ptr2_to_my_function = &my_function;  // explicit ( & is not needed )

    // We can also write the `const` versions:
    //     int (*const ptr3_to_my_function)() = my_function;
    //     const std::function<int(void)> ptr4_to_my_function = my_function;


    // Calls to `my_function`
    cout << "Year: " << (*ptr1_to_my_function)() << "\n";  // explicit deref.
    cout << "Year: " << ptr2_to_my_function() << "\n";     // implicit deref.

    // Verification that both of them "point to" `my_function`
    cout << "Address? " << ptr1_to_my_function << "\n"; // see overload of <<
    cout << "Address! " << reinterpret_cast<void*>(ptr1_to_my_function) << "\n";


    // Transforming a vector...
    vector<int> a;
    for ( int i = 0 ; i < 5 ; i++ )
        a.push_back(i+1);
    cout << "\nVector:\n\t" << a << "\n\n";

    cout << "Squaring ...\n\t";
    // modify_elements_in_vector(a, square_element );
    meiv1(a, square_element );
    cout << a << "\n";

    cout << "Doubling ...\n\t";
    // modify_elements_in_vector(a, double_element );
    meiv2(a, double_element );
    cout << a << "\n";

    cout << "Doubling again...\n\t";
    meiv3(a, double_element );
    cout << a << "\n";

    cout << "And again...\n\t";
    meiv4(a, double_element );
    cout << a << "\n";

    return 0;
}



// Function definitions
ostream& operator<<( ostream& out, int (*f)() ){
    out << reinterpret_cast<void*>(f);
    return out;
}

ostream& operator<<( ostream& out, const vector<int>& v){
    for ( const int& x : v )
        out << x << " ";

    return out;
}

void modify_elements_in_vector( vector<int>& v, int (*some_function)(int) ){
    for ( int& x : v ){
        x = some_function(x);
    }

    return;
}
