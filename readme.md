# Function pointers

This is also an evolving handout. For now it only lists the names of the files
provided in this repository, as well as a short description of the point they
are trying to illustrate.

--- 

Included files:

|**File**|**Description**|**URL**|
|:------------:|:------------------------------------------------|:------------|
| [`readme.md`][read-me] | _readme_ file (`MarkDown` format).| `N/A` |
| [`readme.md.pdf`][read-me-pdf] | _readme_ file (`pdf` format). | `N/A` |
| [`00-basics.cpp`][00-example] | Missing parenthesis in a function call. | [`cpp.sh/4hghx`][00-url] |
| [`01-rei....cpp`][01-example] | Forcing the compiler to display a memory address. | [`cpp.sh/8ddoq`][01-url] |
| [`02-dec....cpp`][02-example] | Function pointer declarations, function calls, and swap attempts. | [`cpp.sh/9gse`][02-url] |
| [`03-fun....cpp`][03-example] | Passing a function to another function. | [`cpp.sh/5eivx`][03-url] |
| [`04-set....cpp`][04-example] | Setting policy via a function pointer. | [`cpp.sh/6qc6u`][04-url] |
| [`05-cus....cpp`][05-example] | "Wrapping" a function pointer. | [`cpp.sh/5j5zj`][05-url] |
| [`06-sim....cpp`][06-example] | A simple, yet useless [incomplete] calculator. | [`cpp.sh/8bnnh`][06-url] |
| [`07-par....cpp`][07-example] | Appending parenthesis to `C++` "things". | [`cpp.sh/5skbk`][07-url] |

[read-me]: readme.md
[read-me-pdf]: readme.md
[00-example]: 00-basics.cpp
[01-example]: 01-reinterpret-cast.cpp
[02-example]: 02-declaration-example.cpp
[03-example]: 03-function-as-parameter.cpp
[04-example]: 04-setting-policy.cpp
[05-example]: 05-custom-types.cpp
[06-example]: 06-simple-calculator.cpp
[07-example]: 07--parenthesis.cpp

[00-url]: http://cpp.sh/4hghx
[01-url]: http://cpp.sh/8ddoq
[02-url]: http://cpp.sh/9gse
[03-url]: http://cpp.sh/5eivx
[04-url]: http://cpp.sh/6qc6u
[05-url]: http://cpp.sh/5j5zj
[06-url]: http://cpp.sh/8bnnh
[07-url]: http://cpp.sh/5skbk
