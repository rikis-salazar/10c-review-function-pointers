// Idea: We can add parenthesis to different things in C++. Let us try to
// understand when this is possible, and whether or not this makes sense.
#include <iostream>
#include <string>
#include <vector>


// Type definitions
typedef int (*function_ptr_t)(void);



// Our old friend
int print_year(void){ return 2017; }



// Custom classes
class CosaSimple{
  public:
    CosaSimple(){
        std::cout << "CosaSimple object instantiated!\n";
    }
};

class CosaNoTanSimple{
  public:
    CosaNoTanSimple(){
        std::cout << "CosaNoTanSimple object instantiated!\n";
    }

    std::string operator()() const {
        return "CosaNoTanSimple pretending to be a function!";
    }
};



// Overloads of `operator<<`
std::ostream& operator<<( std::ostream& out, const CosaSimple& c ){
    out << "CosaSimple object to std::ostream ...";
    return out;
}

std::ostream& operator<<( std::ostream& out, const CosaNoTanSimple& c ){
    out << "CosaNoTanSimple object to std::ostream ...";
    return out;
}

template<typename ItemType>
std::ostream& operator<<( std::ostream& out, const std::vector<ItemType>& v ){
    for ( auto x : v )
        out << x << " ";
    return out;
}

std::ostream& operator<<( std::ostream& out, const std::string& s ){
    const int WIDTH = 10;
    int i = 0;

    out << "\n";
    for ( auto x : s ){
        out << x << " ";

        if ( (++i) % WIDTH == 0 ) 
            out << "\n";
    }
    return out;
}



int main() {
    std::cout << "Appending parenthesis to ..." ;
    std::cout << "\nPrimitive types:"
              << "\n\tint: " << int() 
              << "\n\tdouble: " << double() 
              << "\n\tchar: " << char() << " <-- Invisible zero"
    //        << "\n\tchar: " << static_cast<int>(char()) << " <-- Visible zero"
              << "\n\tbool: " << bool();

    std::cout << "\n\nStandard classes:"
              << "\n\tstring: " << std::string() 
              << "\n\tvector<int>: " << std::vector<int>()
              << "\n\tvector<double>: " << std::vector<double>();
  
    std::cout << "\n\nCustom classes:"
              << "\n\tCosaSimple: " << CosaSimple()
              << "\n\tCosaNoTanSimple: " << CosaNoTanSimple();

    std::cout << "\n\nPointer types:"
    /**
              << "\n\tint*: " << int*()
              << "\n\tint*: " << (int*)()
              << "\n\tdouble*: " << double*()
              << "\n\tdouble*: " << (double*)()
              << "\n\tvoid*: " << (void*)()
              << "\n\tint (*)(void): " << int (*)(void)();
              << "\n\tfunction_ptr_t (aka. int (*)(void): " << function_ptr_t();
    **/
              << "\n\tNo can do!";


    int n = 1;
    double d = 2.34;
    char c = 'c';
    bool t = true;
    std::cout << "\n\nInstances of primitive types (variables):"
    /**
              << "\n\tn(): " << n()
              << "\n\td(): " << n()
              << "\n\tc(): " << n()
              << "\n\tt(): " << n()
    **/
              << "\n\tNo can do!";

    std::string s("Hola");            // s = {'H','o','l','a'}
    std::vector<int> v(3,14);         // v = {14,14,14}
    std::vector<int> w;               // w = {}
    std::cout << "\n\nInstances of standard classes (objects):"
    /**
              << "\n\ts(): " << s() 
              << "\n\tv(): " << v()
              << "\n\tw(): " << w()
    **/
              << "\n\tNo can do!";


    CosaSimple simpleThing;
    std::cout << "\n\nInstances of Custom [simple] classes (objects):"
    /**
              << "\n\t: simpleThing()" << simpleThing() 
    **/
              << "\n\tNo can do!";


    CosaNoTanSimple notSoSimpleThing;
    std::cout << "\n\nInstances of Custom [not so simple] classes (functors):"
              << "\n\t: notSoSimpleThing(): " << notSoSimpleThing() 
              << "\n\tOh yes, can do!";


    int (*f)(void) = print_year;
    function_ptr_t ff = f;
    std::cout << "\n\nInstances of pointer types (variables? objects? functors?):"
              << "\n\tint (*f)(void) = print_year;\t f(): " << f()
              << "\n\tfunction_ptr_t ff = f; \t\tff(): " << ff()
              << "\n\tSuccess!\n";

    return 0;
 }

/** ***************************************************************************
    Output:

Appending parenthesis to ...
Primitive types:
        int: 0
        double: 0
        char:   <-- Invisible zero
        bool: 0

Standard classes:
        string: 

        vector<int>: 
        vector<double>: CosaNoTanSimple object instantiated!
CosaSimple object instantiated!


Custom classes:
        CosaSimple: CosaSimple object to std::ostream ...
        CosaNoTanSimple: CosaNoTanSimple object to std::ostream ...

Pointer types:
        No can do!

Instances of primitive types (variables):
        No can do!

Instances of standard classes (objects):
        No can do!CosaSimple object instantiated!


Instances of Custom [simple] classes (objects):
        No can do!CosaNoTanSimple object instantiated!


Instances of Custom [not so simple] classes (functors):
        : notSoSimpleThing(): 
C o s a N o T a n S 
i m p l e   p r e t 
e n d i n g   t o   
b e   a   f u n c t 
i o n ! 
        Oh yes, can do!

Instances of pointer types (variables? objects? functors?):
        int (*f)(void) = print_year;         f(): 2017
        function_ptr_t ff = f;                 ff(): 2017
        Success!
*************************************************************************** **/
