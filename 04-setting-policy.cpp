// The idea: the behavior of a "receiving" function can change depending on the
// parameter function that is sent. This is known as "setting policy".
#include <iostream>
#include <vector>

using std::cout;
using std::vector;
using std::ostream;

// These will be the functions that set policy.
int square_element( int x ) { return x*x; } 
int double_element( int x ) { return 2*x; }

// In addition, we "fix `cout`" so that it can handle a vector of `int`'s
ostream& operator<<( ostream&, const vector<int>& );

// This is the function that will alter its behavior
void modify_elements_in_vector( vector<int>& , int (*)(int) );


int main(){

    vector<int> a;
    for ( int i = 0 ; i < 5 ; i++ )
        a.push_back(i+1);   // <-- Inefficient way to store `1..5` in a vector.

    // Our vector "via `cout`"
    cout << a << "\n";

    // Now we call `modify...vector(..)`. Depending on which function sets the
    // policy, the elements in the vector are doubled, or squared.
    cout << "Squaring ...\n";
    modify_elements_in_vector(a, square_element );
    cout << a << "\n";
    cout << "Doubling ...\n";
    modify_elements_in_vector(a, double_element );
    cout << a << "\n";

    return 0;
}


// Function definitions
ostream& operator<<( ostream& out, const vector<int>& v){
    for ( const int& x : v )
        out << x << " ";

    return out;
}

void modify_elements_in_vector( vector<int>& v, int (*some_function)(int) ){
    for ( int& x : v ){
        x = some_function(x);
    }

    return;
}
