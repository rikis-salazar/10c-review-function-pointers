// The idea: the compiler tries to make sense of `my_function`, in some cases
// a 1 (`true` boolean value) is displayed, whereas in other cases some random
// hexadecimal number is displayed. We can **force** the compiler to display
// the actual value it receives via a `reinterpret_cast<SomeType>(...)`.

#include <iostream>

using std::cout;

int my_function(){ 
    return 2017;
}

int main(){
    // On some machines the two statements below produce the same results.
    cout << "Year: " 
         << my_function << "\n"; // Oops. I meant to write `my_function()`

    cout << "Year: " 
         << reinterpret_cast<void*>(my_function) << "\n"; 

    /** This is more or less the same idea behind
            int *a = new int[5](); 
            std::cout << a << "\n";       // <-- `a` holds a memory address
            std::cout << &a[0] << "\n";
            delete a;
    **/
    return 0;
}
