// The premise: we code a function that we intend to call, but somehow we fail
// to make the correct call (e.g., we forget the parenthesis). The compiler does
// not complain, and we get a rather funny result.

#include <iostream>

using std::cout;

int my_function(){ // <-- Code starts at some random location (e.g., 0x4011d0)
    return 2017; 
}

int main(){

    cout << "Year: " 
         << my_function << "\n"; // Oops. I meant to write `my_function()`.

    return 0;
}
