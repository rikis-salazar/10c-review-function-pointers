// The idea: We can pass a function to another function. More precisely, we can
// pass a function pointer as a parameter to another function just like we pass
// other parameters [_e.g.,_ variable/object]. The key is to write the correct
// signature for the receiving function, and to realize that, once inside the
// receiving function, we can use parenthesis to make a call to the function
// sent as parameter.

#include <iostream>

using std::cout;

int my_function(){
    return 2017; 
}

// The "receiving" function in this case will be `cout`. More precisely, we
// overload `operator<<` so that it receives a pointer to a function. Once
// inside this receiving function, we
//     1) make a call to the function parameter, and
//     2) display the memory location stored in the function parameter.
//
// Here is the signature of the receiving function:
std::ostream& operator<<( std::ostream&, int (*)() );

// Notice that there is no need to "name" the parameters. Hence the "funny"
// syntax, especially in the second parameter.

int main(){

    // Info displayed without "passing `my_function` directly to `cout`"
    cout << "The function at " << reinterpret_cast<void*>(my_function) 
         << ", when called returns the value " << my_function() << ".\n"; 

    // Info displayed directly via `cout`
    cout << my_function;
   return 0;
}


// Finally, we proceed to define the overload of `operator<<`.
// Recall that above we use the signature:
//     std::ostream& operator<<( std::ostream&, int (*)() );
//
// Here in the definition, we name the parameters `out`, and `f`.
std::ostream& operator<<( std::ostream& out, int (*f)() ){
    cout << "The function at " << reinterpret_cast<void*>(f) 
         << ", when called returns the value " << f() << ".\n"; 
    return out;
}
