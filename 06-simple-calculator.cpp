// Idea: We can use function pointers to code a calculator. Only this time we'll
// use one function to perform all computations. The behavior of this function
// is determined by the second parameter.
#include <iostream>


// Typically these functions return the same type they receive. In this case we
// return a different type just for the fun of it, and to avoid having only
// `int`'s in the pointer type definition below.
double addition(int a, int b) { return static_cast<double>(a+b); }
double multiplication(int a, int b) { return static_cast<double>(a*b); }


// We'll use (read define) a function pointer type for functions that take two
// `int`'s and return a `double`
typedef double (*function_ptr_t)( int, int );


// This function we'll be called regardless of the value stored in the variable
// `choice` (see `main` below). 
double perform_operation( int a, int b, function_ptr_t f ){
   return f(a,b);
}



int main(){
    // The values to be used in the operations
    int x = 3;
    int y = 5;
    std::cout << "x = " << x << ", y = " << y << "\n";

    // The user generally gets to choose the operation...
    //     std::cout << "Choice?\n\t1: x + y\n\t2: x * y\n";
    // ... but we do not really care about said user here!
    int choice = 2;

    std::cout << "Result: " ;
    switch( choice ){
        case 1:
            std::cout << perform_operation(x,y,addition) << "\n";
            break;
        case 2: 
            std::cout << perform_operation(x,y,multiplication) << "\n";
            break;
        // Care to complete this calculator? We are missing division and
        // subtraction operators.
    }

   return 0;
}
